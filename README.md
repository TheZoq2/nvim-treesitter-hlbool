# nvim-treesitter-hlbool

Highlights true and false literals in a few languages to make them
differentiable at a glance

Requires adding a highlight group for `@bool.true` and `@bool.false`
